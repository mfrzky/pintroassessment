import { Sidebar, Menu, MenuItem, SubMenu} from 'react-pro-sidebar';
import { HomeRounded, DarkModeRounded, LocalLibraryRounded, AssistantRounded, StarRounded, BlindRounded, MoneyOutlined, Telegram, SupervisedUserCircleRounded } from '@mui/icons-material';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';

import './App.css';
import Routes from './routes';
import Navbar from './views/Navbar';
import Dashboard from './views/Dashboard';
import Login from './views/Login';

function App() {
  const [collapsed, setCollapsed] = useState(true);

  const [windowDimension, setWindowDimension] = useState(null);

  useEffect(() => {
    setWindowDimension(window.innerWidth);
  }, []);

  useEffect(() => {
    function handleResize() {
      setWindowDimension(window.innerWidth);
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const isMobile = windowDimension <= 640;
  return (
    <>
       {window.location.pathname !== '/login' ? 
       isMobile ? 
        <div style={{ display: "flex", height: "100vh" }}>
        <nav class="navbar fixed-bottom navbar-light" style={{backgroundColor: '#072e6b', color:'white'}}>
          <div class="container-fluid">
            <div className='text-center'>
              <i className='d-block'>{<HomeRounded fontSize='small'/>}</i>
              <button className='btn border-0 d-block py-0 fs-6 text-white'>Home</button>
            </div>
            <div className='text-center'>
              <i className='d-block'>{<MoneyOutlined fontSize='small'/>}</i>
              <button className='btn border-0 d-block py-0 fs-6 text-white'>Transaksi</button>
            </div>
            <div className='text-center'>
              <i className='d-block'>{<Telegram fontSize='small'/>}</i>
              <button className='btn border-0 d-block py-0 fs-6 text-white'>Tugas</button>
            </div>
            <div className='text-center'>
              <i className='d-block'>{<SupervisedUserCircleRounded fontSize='small'/>}</i>
              <button className='btn border-0 d-block py-0 fs-6 text-white'>Akun</button>
            </div>
          </div>
        </nav>
        <div className='container-fluid p-0 ms-lg-5'>
          <Dashboard/>
        </div>
        <Routes/>
        </div> :
        <div style={{ display: "flex", height: "100vh" }}>
          <div style={{ display: "flex", height: "100vh", position: 'fixed', zIndex: '1001'}} onMouseEnter={() => setCollapsed(false)} onMouseLeave={() => setCollapsed(true)}>
            <Sidebar collapsed={collapsed} backgroundColor='#072e6b'>
              <Menu menuItemStyles={{button: {
                '&:hover': {
                  backgroundColor: '#0c2654',
                }}}}
              >
                {!collapsed ? <h2 className='text-white ms-4 mt-3'>Pintro</h2> : <h2 className='d-flex justify-content-center text-white mt-3'>P</h2>}
                
                <MenuItem className='text-white' icon={<HomeRounded/>} component={<Link to='/'/>}> Home </MenuItem>
                <SubMenu className='text-white' label="Program Boarding" icon={<DarkModeRounded/>}>
                  <MenuItem className='text-secondary'> Mengaji Online </MenuItem>
                  <MenuItem className='text-secondary'> Tahfidzh </MenuItem>
                  <MenuItem className='text-secondary'> Jadwal Kegiatan </MenuItem>
                </SubMenu>
                <SubMenu className='text-white' label="Kegiatan Belajar" icon={<LocalLibraryRounded/>}>
                  <MenuItem className='text-secondary'> Mengaji Online </MenuItem>
                  <MenuItem className='text-secondary'> Tahfidzh </MenuItem>
                  <MenuItem className='text-secondary'> Jadwal Kegiatan </MenuItem>
                </SubMenu>
                <MenuItem className='text-white' icon={<AssistantRounded/>}> Penilaian </MenuItem>
                <MenuItem className='text-white' icon={<StarRounded/>}> Ekstrakulikuler </MenuItem>
                <MenuItem className='text-white' icon={<BlindRounded/>} component={<Link to='/login'/>}> To Login </MenuItem>
              </Menu>
            </Sidebar>
          </div>
          <div className='container-fluid p-0 ms-5'>
            <Navbar/>

            <Dashboard/>
          </div>
          <Routes/>
        </div>
        : <Login/>}
    </>
    
  );
}

export default App;
