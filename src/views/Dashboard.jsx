import { useEffect, useState } from "react";

export default function Dashboard() {
    const [windowDimension, setWindowDimension] = useState(null);

    useEffect(() => {
        setWindowDimension(window.innerWidth);
    }, []);

    useEffect(() => {
        function handleResize() {
        setWindowDimension(window.innerWidth);
        }

        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    }, []);

    const isMobile = windowDimension <= 640;
    return(
        <>
            <div className="mx-5">
                <div className="row">
                    <div className="col-lg-8 mt-lg-0 mt-3 align-self-end">
                        <h1 className="fw-light">
                            Selamat Datang, <span className="fw-bolder">Ririh!</span>
                        </h1>
                        <span className="fw-bolder text-success">
                            Kamu memiliki <span className="text-info">3 Tugas </span> belum dikumpul, dan <span className="text-danger">1 Transaksi </span> menunggu pembayaran
                        </span>
                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-6 mt-lg-5 mt-3">
                                <div className="card h-100 rounded-4 border-0 shadow-sm">
                                    <div className="card-body">
                                        <div className="mb-5"></div>
                                        <div>
                                            <h3>Ririh Kantilaras</h3>
                                            <span className="text-info fw-bolder">SMPN 1 Setiabudi &#x2022; 836239237</span>
                                        </div>
                                        <div className="mt-3">
                                            <h6>Geraham Matahari Lorem Ipsum 2B</h6>
                                            <span className="text-info fw-bolder">Kelas</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 mt-lg-5 mt-3">
                                <div className="card h-100 rounded-4 border-0 shadow-sm">
                                    <div className="card-body">
                                        <div>
                                            <h3>Tugas</h3>
                                            <span className="text-info fw-bolder">Jangan Lewatkan Tugas</span>
                                        </div>
                                        <div className="mt-3">
                                            <h6>Tugas Bab 1</h6>
                                            <span className="text-info fw-bolder">Bpk Sumardi</span>
                                        </div>
                                        <div className="mt-3">
                                            <h2 className="text-danger">3 April 2021</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 mt-lg-0 mt-md-5 mt-3">
                        <div className="card h-100 rounded-4 border-0 shadow-sm">
                            <div className="card-body p-4">
                                <div className="card bg-success">
                                    <div className="card-title p-3">
                                        <h2 className="m-0 text-white">September 2021</h2>
                                    </div>
                                </div>

                                <div className="row mt-4">
                                    <div className="col-lg-2 text-lg-end text-md-start text-start">
                                        <h4>Sen</h4>
                                        <h4>23</h4>
                                    </div>
                                    <div className="col">
                                        <h6 className="border-0 shadow-sm bg-success text-white p-3 rounded-4">Kunjungan ke museum dinosaurus</h6>
                                        <h6 className="border-0 shadow-sm bg-success text-white p-3 rounded-4">Pengumpulan tugas tematik</h6>
                                        <h6 className="border-0 shadow-sm bg-success text-white p-3 rounded-4">Kelas online bersama Ibu Jamila</h6>
                                    </div>
                                </div>

                                <div className="row mt-4">
                                    <div className="col-lg-2 text-lg-end text-md-start text-start">
                                        <h4>Sel</h4>
                                        <h4>24</h4>
                                    </div>
                                    <div className="col">
                                        <h6 className="border-0 shadow-sm bg-warning text-white p-3 rounded-4">Kelas online bersama Ibu Jamila</h6>
                                        <h6 className="border-0 shadow-sm bg-warning text-white p-3 rounded-4">Upacara Perpisahan</h6>
                                    </div>
                                </div>

                                <div className="d-flex justify-content-between mt-5">
                                    <h6>Hari ini</h6>
                                    <h6 className="text-info">25-27 &#10095;</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="mx-5 mt-lg-5 mt-3">
                <div className="row">
                    <div className="col-lg-8">
                        <div className="card h-75 rounded-4 border-0 shadow-sm">
                            <div className="card-body">
                                <h3>Agenda Lorem Ipsum</h3>
                                <span className="fw-bolder text-info">Dibimbing oleh Bpk Sumardi</span>

                                <ol className="ps-3 my-5">
                                    <li>Please, download a video below and watch it</li>
                                    <li>JUMP ROPE (15 Second) activities</li>
                                    <li>Take a picture or video when doing it</li>
                                </ol>

                                <h3>3 Apr 2021 23:00</h3>
                            </div>
                        </div>
                        <div className="row mt-3 mt-lg-5 mb-5 mb-lg-0 mb-md-0 align-self-end">
                            <div className="col">
                                <div className="card h-100 rounded-4 border-0 shadow-sm">
                                    <div className="card-body">
                                        <span className="fw-bold">Kegiatan Traning Fitur HR di Yayasan Syiar Bangsa</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card h-100 rounded-4 border-0 shadow-sm">
                                    <div className="card-body">
                                        <span className="fw-bold">Kegiatan Traning Fitur HR di Yayasan Syiar Bangsa</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 mt-5 mt-lg-0 mt-md-3">
                        <div className="card h-100 rounded-4 border-0 shadow-sm">
                            <div className="card-body p-4">
                                <div className="d-flex justify-content-between">
                                    <h3 className="m-0">Jadwal Hari Ini</h3>
                                    <select className="form-select w-25">
                                        <option>Kemarin</option>
                                        <option selected>Hari Ini</option>
                                        <option>Besok</option>
                                    </select>
                                </div>

                                <div className="row mt-3 mt-lg-5">
                                    <div className="col">
                                        <span className="d-block fw-bold">Pendidikan Agama</span>
                                        <span className="text-info d-block">E312 &#x2022; 08.00 - 10.00</span>
                                    </div>
                                </div>
                                <div className="row mt-3 mt-lg-5">
                                    <div className="col">
                                        <span className="d-block fw-bold">Matematika</span>
                                        <span className="text-info d-block">E312 &#x2022; 08.00 - 10.00</span>
                                    </div>
                                </div>
                                <div className="row mt-3 mt-lg-5">
                                    <div className="col">
                                        <span className="d-block fw-bold">Olahraga</span>
                                        <span className="text-info d-block">E312 &#x2022; 08.00 - 10.00</span>
                                    </div>
                                </div>
                                <div className="row mt-3 mt-lg-5">
                                    <div className="col">
                                        <span className="d-block fw-bold">Pendidikan Agama</span>
                                        <span className="text-info d-block">E312 &#x2022; 08.00 - 10.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* Footer */}
            <div className="mx-5">
                <hr className="border-1 text-secondary"/>
                <div className="d-flex justify-content-between">
                    <span className="fw-bold">COBRAND</span>
                    <span className="fw-bold mb-5">&#169; Jakarta 2021. All Right Reserved</span>
                    <span className="fw-bold">PINTRO</span>
                </div>
            </div>
            {isMobile ? <> <br /> <br /> </> : ''}
        </>
    )
}