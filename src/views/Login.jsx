import { useEffect, useState } from 'react';
import '../App.css';

export default function Login() {
    const [windowDimension, setWindowDimension] = useState(null);

    useEffect(() => {
        setWindowDimension(window.innerWidth);
    }, []);

    useEffect(() => {
        function handleResize() {
        setWindowDimension(window.innerWidth);
        }

        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    }, []);

    const isMobile = windowDimension <= 640;

    return(
        <>
        {isMobile ? 
            <div className="bg-white">
                <div className="vh-100 d-flex justify-content-center align-items-center">
                    <div className="container">
                        <div className="row d-flex justify-content-center">
                            <div className="col-12 col-md-8 col-lg-6">
                                <h3 className="fw-bold mb-2">PintroPay</h3>
                                <div className="card border-0 bg-white">
                                    <div className="card-body p-0">
                                        <h3 className="fw-bold mb-2">One Payment Dashboard</h3>
                                        <p className=" mb-5">Tempat terbaik untuk melihat dan mengukur kuantitas transaksi lembaga Anda.</p>
                                        <div className="mb-3">
                                            <label for="email" className="form-label ">Email</label>
                                            <input type="email" className="form-control" id="email" name="email"/>
                                        </div>
                                        <div className="mb-1">
                                            <label for="password" className="form-label ">Password</label>
                                            <input type="password" className="form-control" id="password" name="password"/>
                                        </div>

                                        <div className="mb-3">
                                            <a href="/#" className="fw-bold text-primary text-decoration-none">Lupa Password?</a>
                                        </div>

                                        <div className="d-grid">
                                            <a href='/' className="btn btn-primary text-white fw-bold rounded-3" type="submit">Masuk</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> : 
            <div className="row m-0">
                <div className="col-6">
                    <div id="carouselExampleDark" className="carousel carousel-dark slide h-100" data-bs-ride="carousel">
                        <div className="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" className="active buttonIndicator" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" className="buttonIndicator" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" className="buttonIndicator" aria-label="Slide 3"></button>
                        </div>
                        <div className="carousel-inner">
                            <div className="carousel-item active" data-bs-interval="3000">
                                {/* <img src="https://play-lh.googleusercontent.com/pK6ngV5kVVqpe3j54nODTXH9flvBUql0j_rYdITpOteUpx4WjDzTjwiTypRAj6CmHX_9" className="d-block"/> */}
                                <div className="text-center vh-100 d-flex justify-content-center align-items-center">
                                    <div>
                                        <h5 className='d-block'>Rekonsiliasi keuangan yang lebih mudah</h5>
                                        <p className='d-block'>Setiap transaksi akan tercatat otomatis dalam sistem pembayaran dengan cepat & tranparan</p>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item" data-bs-interval="3000">
                                {/* <img src="https://play-lh.googleusercontent.com/pK6ngV5kVVqpe3j54nODTXH9flvBUql0j_rYdITpOteUpx4WjDzTjwiTypRAj6CmHX_9" className="d-block"/> */}
                                <div className="text-center vh-100 d-flex justify-content-center align-items-center">
                                    <div>
                                        <h5 className='d-block'>Mendukung 25+ channel pemabayaran</h5>
                                        <p className='d-block'>Tersedia lebih dari 25 channel pembayaran yang dapat meningkatkan kepuasan saatt bertransaksi</p>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item">
                                {/* <img src="https://play-lh.googleusercontent.com/pK6ngV5kVVqpe3j54nODTXH9flvBUql0j_rYdITpOteUpx4WjDzTjwiTypRAj6CmHX_9" className="d-block"/> */}
                                <div className="text-center vh-100 d-flex justify-content-center align-items-center">
                                    <div>
                                        <h5 className='d-block'>Tingkatkan penerimaan dengan transaksi online</h5>
                                        <p className='d-block'>Cek dan update transaksi harian maupun bulanan mudah dalam genggaman</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-6 bg-white">
                <div className="vh-100 d-flex justify-content-center align-items-center">
                    <div className="container">
                        <div className="row d-flex justify-content-center">
                            <div className="col-12 col-md-8 col-lg-6">
                                <h3 className="fw-bold mb-2">PintroPay</h3>
                                <div className="card border-0 bg-white">
                                    <div className="card-body p-0">
                                        <h3 className="fw-bold mb-2">One Payment Dashboard</h3>
                                        <p className=" mb-5">Tempat terbaik untuk melihat dan mengukur kuantitas transaksi lembaga Anda.</p>
                                        <div className="mb-3">
                                            <label for="email" className="form-label ">Email</label>
                                            <input type="email" className="form-control" id="email" name="email"/>
                                        </div>
                                        <div className="mb-1">
                                            <label for="password" className="form-label ">Password</label>
                                            <input type="password" className="form-control" id="password" name="password"/>
                                        </div>

                                        <div className="mb-3">
                                            <a href="/#" className="fw-bold text-primary text-decoration-none">Lupa Password?</a>
                                        </div>

                                        <div className="d-grid">
                                            <a href='/' className="btn btn-primary text-white fw-bold rounded-3" type="submit">Masuk</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="position-absolute align-self-end text-center mb-2">
                        <p>Hak Cipta &#169; Pintro - 2023. All rights reserved.</p>
                        <p>Kontak Kami | Privacy Policy | Term of Use | Data Retention Policy</p>
                    </div>
                </div>
                </div>
            </div>
        }
        </>
    )
}