import { PersonRounded, KeyboardArrowDownRounded, NotificationsRounded } from '@mui/icons-material';

export default function Navbar() {
    return(
        <div className="border-bottom shadow-sm p-3 mb-5 bg-body rounded">
            <nav className="d-flex justify-content-between align-items-center mx-5">
                <div className="">
                    <h1 className="m-0">Pintro</h1>
                </div>
                <div className="float-right">
                    <div className="d-flex align-items-center">
                        <i className='me-3'><NotificationsRounded/></i>
                        <i className='me-3'><PersonRounded/></i>
                        <div>
                            <span className="fw-bold d-block">Ririh Kantilaras</span>
                            <span className="d-block">SMPN 1 Setiabudi</span>
                        </div>
                        <i className='ms-3'><KeyboardArrowDownRounded/></i>
                    </div>
                </div>
            </nav>
        </div>
    )
}